﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListBoxItemWithToolTip
{
	public partial class Form1 : Form
	{
		/// <summary>
		/// current item index of ListBox via mouse move cursor location
		/// <para>** -1 means no item matched **</para>
		/// </summary>
		private int _currentItemIndexOfListBoxViaMouseMove = -1;

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			listBox1.MouseMove += listBox1_MouseMove;
			listBox1.MouseLeave += listBox1_MouseLeave;
		}

		void listBox1_MouseMove(object sender, MouseEventArgs e)
		{
			var listbox = sender as ListBox;

			//find item index of ListBox Items via mouse location
			int itemIdx = listbox.IndexFromPoint(e.Location);

			//set the tooltip message
			var tooltipMsg = (itemIdx > -1) ? listbox.Items[itemIdx].ToString() : "";

			//set ToolTip message, the condition is to avoid setToolTip redundantly.
			if (_currentItemIndexOfListBoxViaMouseMove != itemIdx)
			{
				_currentItemIndexOfListBoxViaMouseMove = itemIdx;
				toolTip1.SetToolTip(listbox, tooltipMsg);
			}
		}

		void listBox1_MouseLeave(object sender, EventArgs e)
		{
			// -1 means no item matched
			_currentItemIndexOfListBoxViaMouseMove = -1;
		}
	}
}
